# Enrutamiento y Peticiones HTTP.

Para esta sesión, estaremos realizando peticiones a un web service, con el objetivo de conseguir datos y mostrarlos desde nuestra aplicacion web.

Como el curso abarca únicamente temas de frontend, harémos uso de el RESTfull [**PokéAPI**](https://pokeapi.co/), el cual hará el papél de _servidor_, y de éste obtendremos la información de un conjunto de pokemones, o de un pokemon en especifico cuando lo deseemos.

Para empezar, crearemos un nuevo proyecto a partir de la terminal

```bash
$ vue create pokedex
```
Desde ahi mismo, incluimos *Vuex* y *router*

Ahora, usaremos la interfaz gráfica de vue para instalar vuetify.

```bash
$ vue ui
```

![vue ui](./ui.png)

Ya con vuetify instalado, procedemos a borrar todo lo que está en `src/components` y en `src/views`.

Empezarémos creando el componente `BarraSuperior.vue` en la carpeta components, éste será un componente visible desde todas las vistas de nuestra aplicación.

```HTML
<template>
  <v-toolbar class="red lighten-2">
    <router-link :to="{ name: 'home' }">
      <img id="logo-image" src="../assets/logo.png" alt="">
    </router-link>
    <v-card id="card">
      <input type="text" name="busqueda"  v-model="busqueda" @keyup.enter="search()">
      <v-icon>search</v-icon>
    </v-card>
    <v-input></v-input>
  </v-toolbar>
</template>

<script>
import PokemonService from '@/services/PokemonServices'
export default {
  data(){
    return {
      busqueda: null
    }
  },
}
</script>

<style scoped>
#logo-image {
  max-width: 5em;
  max-height: 5em;
  margin-top: 5px;
}
input {
  margin: 5px;
}
#card {
  margin-left: 2em;
}
</style>
```

En ésta barra tendremos el logo pokemon, el cual nos llevará a nuestra ventana principal, y ademas tendrá un buscador, que por ahora solo lo dejaremos visible, pero no funciona.

Ahora, editaremos el archivo `App.vue`, agregando el componente que acabamos de crear, y le agregaremos un componente llamado **_router view_**.

```HTML
<template>
  <v-app>
    <barra-superior></barra-superior>
    <v-content>
      <v-layout>
        <v-flex xs10 offset-xs1>
          <router-view></router-view>
        </v-flex>
      </v-layout>
    </v-content>
  </v-app>
</template>
<script>
import BarraSuperior from './components/BarraSuperior'

export default {
  name: 'App',
  components: {
    BarraSuperior
  },
  data () {
    return {}
  }
}
</script>
```

La función del **_router view_** es la de móstrar el componente que indiquemos. Para entender mejor ésto, es necesario ir al archivo router.js, el cual se creó por defecto.

```HTML
import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode:'history',
  routes: [

  ]
});
```

El único cambio que realizamos hasta ahora en este archivo ha sido el `mode:'history'`, el cual permite historial en navegadores que admitan HTML5.

Éste modo requiere de unas configuraciones extras en el modo de producción.

En la sección `routes:[]` incluiremos todas nuestras urls, las cuales simplemente son otro componente de vue.

Por ahora crearemos nuestra primer vista `views/Home.vue`.
```HTML
<template>
  <lista-pokemon></lista-pokemon>
</template>

<script>
import ListaPokemon from '@/components/ListaPokemon'
export default {
  components: {
    ListaPokemon
  }
}
</script>
```
>Se puede observar que el componente BarraSuperior.vue y la vista Home.vue tienen la misma extención, y es por que ambos son componentes de vue, pero entonces por que los ubicamos en lugares diferentes?
>>Por organización, es mejor manejar lo que son las vistas, en un directorio, y desde éstas vistas cargar componentes reutilizables alojados en el directorio `/components`.

En esta vista solo deseamos mostrar un componente llamado **ListaPokemon**, el cual procemenos a crear inmediatamente.
```HTML
<template>
  <v-layout wrap>
    <template v-for="pokemon in pokemones">
      <v-flex xs4>
        <v-card @click="irPokemon()">
          <v-layout align-center>
            <v-flex class="pokemon-image">
              <img alt="">
            </v-flex>
            <v-flex>
              {{ pokemon.name }}
            </v-flex>
          </v-layout>
        </v-card>
      </v-flex>
    </template>
  </v-layout>
</template>

<script>
import servicePokemon from '@/services/PokemonServices.js'

export default {
  data(){
    return {
      pokemones: []
    }
  },
  methods: {
    irPokemon(){

    }
  }
}
</script>
```
El componente ListaPokemon, se encargará de renderizar la foto y el nombre de un número de pokemones especificados en la lista `pokemones:[]`, en éste momento no tenemos pokemones, por lu cual se usará Axios.

Axios es una libreria para peticiones asincronas, con lo cual podremos acceder a los datos que deseamos.

Para ésto, se creará un directorio `src/services` el cual contendra nuestras conexiones.

Crearemos en el directorio recien creado un archivo llamado `PokemonServices.js`

```HTML
import axios from 'axios'

const apliClient = axios.create({
  baseURL: 'https://pokeapi.co/api/v2/',
  withCredentials: false,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

export default {
  getPokemones(){
    return apliClient.get('/pokemon')
  },
  getPokemon(id){
    return apliClient.get('/pokemon/'+id)
  }
}
```
importamos axios y lo inicializamos, la gracia es tener una única instancia de axios, por lo cual es recomendable en este mismo script manejar las peticiones, por lo cual se crearon 2 métodos getPokemones, el cual trae una lista de pokemones, y el método getPokemon, el cual trae información detallada de un único pokemon.

Ahora, volviendo a ListaPokemon, agregaremos un par de lineas para usar PokemonService,

```HTML
methods:{...},
created(){
  servicePokemon.getPokemones()
  .then(response => {
    this.pokemones = response.data.results
  })
},
```
Creamos una nueva sección llamada created, la cual se llamará cada vez que el componente sea creado.

De esta manera, estamos cargando el arreglo de pokemones con lo que llega de nuestra petición.

Por la forma en que el servidor trae los datos, es necesario ajustarlos para poder usarlos desde nuestro front.

Ademas, observando mas a fondo los mismos datos, se observa existen imagenes de los pokemones en la url 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/', por lo cual nos será de ayuda para mostrar las respectivas imagenes de los pokemones.

Así quedaria entonces nuestro componente.

```HTML
<template>
  <v-layout wrap>
    <template v-for="pokemon in pokemones">
      <v-flex xs4 :key="pokemon.url.split('/')[pokemon.url.split('/').length-2]">
        <v-card @click="irPokemon(pokemon.url.split('/')[pokemon.url.split('/').length-2])">
          <v-layout align-center>
            <v-flex class="pokemon-image">
              <img :src="link+pokemon.url.split('/')[pokemon.url.split('/').length-2]+'.png'" alt="">
            </v-flex>
            <v-flex>
              {{ pokemon.name }}
            </v-flex>
          </v-layout>
        </v-card>
      </v-flex>
    </template>
  </v-layout>
</template>

<script>
import servicePokemon from '@/services/PokemonServices.js'

export default {
  data(){
    return {
      link: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/',
      pokemones: []
    }
  },
  created(){
    servicePokemon.getPokemones()
    .then(response => {
      this.pokemones = response.data.results
    })
  },
  methods: {
    irPokemon(id){
      this.$router.push('/pokemon/'+id)
    }
  }
}
</script>
```
Ademas hemos creado el metodo irPokemon, el cual irá a una ruta que no hemos creado, y seria la que mostraria información especifica de un pokemon.

Creemos esta vista en `src/views/Pokemon.vue`

En esta vista solo mostraremos las imagenes disponibles de cada pokemon, y las caracteristicas del pokemon.
```HTML
<template>
  <v-flex xs8 offset-xs2>
    <br>
    <v-card>
      <v-card-title>
          <h1>{{ nombre }}</h1>

      </v-card-title>
      <v-card-text>
        <v-layout wrap>
          <v-flex xs8>
            <v-carousel class="carrusel">
              <v-carousel-item v-for="sprite in Object.values(sprites)" :src="sprite" v-if="sprite">
              </v-carousel-item>
            </v-carousel>
          </v-flex>
          <v-flex xs4>
            <ul>
              <li v-for="tipo in tipos">{{tipo}}</li>
            </ul>
            <p v-if="error">error</p>
          </v-flex>
        </v-layout>

      </v-card-text>
    </v-card>
  </v-flex>

</template>

<script>
import PokemonService from '@/services/PokemonServices'
export default {
  props: ['id'],
  data(){
    return {
      nombre: '',
      sprites:[],
      tipos: [],
      data: [],
      error: null
    }
  },
  methods:{
    obtenerPokemon(id){
      PokemonService.getPokemon(id)
        .then(response => {
          this.nombre = response.data.name
          this.sprites = response.data.sprites
          this.data = response.data.types
          this.tipos = []
          for (let i=0, len=response.data.types.length; i < len; i++ ){
            this.tipos.push(response.data.types[i].type.name)
          }
    })
    }
  },
  created(){
    this.obtenerPokemon(this.id)
  },
  watch:{
    id(){
      this.obtenerPokemon(this.id)
    }
  }

}
</script>
```
Observamos que hemos agregado a éste componente el elemento _props_, lo que significa que como parte de la ruta llegara un atributo del cual haremos uso, en este caso un id, con el cual podemos identificar al pokemon que estamos mostrando, el cual llegará por la url y se explicará con más claridad gracias al siguiente fragmento de código.


Por último, solo hace falta actualizar nuestro archivo de rutas, agregando en la seccion de `routes:[]` lo siguiente.

```js
{
  path: "/",
  name: "home",
  component: Home
},
{
  path: "/pokemon/:id",
  name: "pokemon",
  props: true,
  component: Pokemon
}
```
se puede observar en la segunda ruta, en el path hay un elemento `:id` y `props: true,` Esto significa que realizaremos enrutamiento dinámico, osea que la url recibe un id como atributo, con lo cual podemos desplegar información del pokemon 1, 102, o cualquiera que podamos obtener su id. Ésto se complementa con


Y para finalizar, darémos funcionalidad a la barra superior, definiendo el metodo search
```js
methods:{
  search(){
    PokemonService.getPokemon(this.busqueda)
    .then(response=>{
        this.$router.push('/pokemon/'+response.data.id)
    })

  }
}
```
