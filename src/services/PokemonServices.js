import axios from 'axios'

const apliClient = axios.create({
  baseURL: 'https://pokeapi.co/api/v2/',
  withCredentials: false,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

export default {
  getPokemones(pag=1,perpage=20){
    return apliClient.get('/pokemon?offset='+(perpage*(pag-1))+'&limit='+perpage)
  },
  getPokemon(id){
    return apliClient.get('/pokemon/'+id)
  }
}
